export const state = () => ({
    message: ''
});

export const mutations = {
    setMessage(state, message) {
        state.message = message;
    }
}

export const actions = {
    getMessage(context) {
        const message = getCookie('message');
        context.commit('setMessage', message);
    }
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}